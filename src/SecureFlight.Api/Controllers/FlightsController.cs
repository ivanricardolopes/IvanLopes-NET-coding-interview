﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IFlightRepository _flightRepository;
    private readonly IPassangerRepository _passangerRepository;

    public FlightsController(IService<Flight> flightService, IMapper mapper,
        IFlightRepository flightRepository, IPassangerRepository passangerRepository)
        : base(mapper)
    {
        _flightService = flightService;
        _flightRepository = flightRepository;
        _passangerRepository = passangerRepository;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();

        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet]
    [Route("/GetByOrigin")]
    public async Task<IActionResult> Get([FromQuery] string origin, string destination)
    {
        var flights = await _flightRepository.GetByOriginDestinationAsync(origin, destination);

        return Ok(flights);
    }


    [HttpPut]
    public async Task<IActionResult> AddPassenger(string code, string passengerId)
    {
        Flight flight = await _flightRepository.GetByCodeAsync(code);

        Passenger passenger = await _passangerRepository.GetByIdAsync(passengerId);

        flight.AddPassenger(passenger);

        _flightRepository.Update(flight);

        return Ok();
    }



}