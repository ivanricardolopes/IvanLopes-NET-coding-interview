﻿namespace SecureFlight.Api.Models;

public record AirportDataTransferObject
{
    public string Code { get; init; } = default;

    public string Name { get; init; }

    public string City { get; init; }

    public string Country { get; init; }
}