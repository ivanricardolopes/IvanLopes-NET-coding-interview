﻿using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SecureFlight.Infrastructure.Repositories
{
    ///<inheritdoc/>
    public class PassegenderRepository : BaseRepository<Passenger>, IPassangerRepository
    {
        private readonly SecureFlightDbContext _context;

        public PassegenderRepository(SecureFlightDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Passenger> GetByIdAsync(string id)
        {
            return await _context.Passengers
                .FirstOrDefaultAsync(p => p.Id == id);
        }

    }
}
