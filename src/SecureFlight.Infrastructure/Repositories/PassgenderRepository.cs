﻿using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SecureFlight.Infrastructure.Repositories
{
    ///<inheritdoc/>
    public class FlightRepository : BaseRepository<Flight>, IFlightRepository
    {
        private readonly SecureFlightDbContext _context;

        public FlightRepository(SecureFlightDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<Flight> GetByCodeAsync(string code)
        {
            return await _context.Flights
                .Include(f => f.Passengers)
                .FirstOrDefaultAsync(p => p.Code == code);
        }

        public async Task<Flight> GetByOriginDestinationAsync(string origin, string destination)
        {
            return await _context.Flights
                .FirstOrDefaultAsync(p => p.OriginAirport == origin
                && p.DestinationAirport == destination);
        }
    }
}
