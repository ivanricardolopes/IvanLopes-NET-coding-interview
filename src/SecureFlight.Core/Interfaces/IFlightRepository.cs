﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    /// <summary>
    /// Represents a repository for managing flights.
    /// </summary>
    /// <typeparam name="Flight">The type of flight entity.</typeparam>
    public interface IFlightRepository : IRepository<Flight>
    {
        /// <summary>
        /// Asynchronously retrieves a flight based on its unique code.
        /// </summary>
        /// <param name="code">The unique code of the flight to retrieve.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the flight with the specified code, or null if not found.</returns>
        Task<Flight> GetByCodeAsync(string code);

        Task<Flight> GetByOriginDestinationAsync(string origin, string destination);
    }

}
