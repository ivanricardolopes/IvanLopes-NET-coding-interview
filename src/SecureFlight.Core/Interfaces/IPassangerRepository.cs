﻿using SecureFlight.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IPassangerRepository : IRepository<Passenger>
    {
        Task<Passenger> GetByIdAsync(string id);
    }

}
